import { Fragment, useRef } from "react"
import { useState } from "react"
import classes from './Header.module.css'
const Header =(props)=>{

    const [enteredUsername, setUsername] =useState()
    const [age, setage]= useState()
    const [userList, setUserList]= useState([])
    const addUserHandler=(event)=>{
        event.preventDefault()
        setUserList((previousList)=>{
            return [...previousList,{name:enteredUsername,age:age}]
        })
        // setUserList([...{name:enteredUsername,age:age}])
    }
    const usernameHandler=(event)=>{
        setUsername(event.target.value)
    }
    const ageHandler=(event)=>{
        setage(event.target.value)
    }
   
    return (
        <div id="">
            <form onSubmit={addUserHandler}>
            <label> userName </label>
            <input id="username" value ={enteredUsername} onChange={usernameHandler}type="text"></input>
            <label> age </label>
            <input id="age" value ={age} onChange={ageHandler} type="number"></input>
            <button type="submit">addUser</button>
            </form>
            <ol>
            {userList.map((user,index)=>{
                return <li key={index}> {user.name}{user.age}</li>
            })}
        </ol>
        </div>
    )
}

export default Header